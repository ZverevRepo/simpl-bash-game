#!/bin/bash
echo "Ходить клавишами (w,a,s,d)"

sleep 1

clear

    # | # - Hero
    # | E - Enemy
    x00=("#" "E" "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x01=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x02=("." "." "E" "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x03=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x04=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x05=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x06=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x07=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x08=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x09=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x10=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x11=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x12=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x13=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." ".")
    x14=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "E" ".")
    x15=("." "." "." "." "." "." "." "." "." "." "." "." "." "." "." "W")


    #              x   y
    locationHero=("0" "0")
    heroX=" "
    heroY=" "

    lifeNow=("(#)" "(#)" "(#)")

    enemy_1=("1" "0")
    enemy_2=("2" "2")
    enemy_3=("14" "14")

drow(){
    for j in x{00..15}
    do
        for i in {0..15}
        do
            case $j in
                x00)
                    echo -n "${x00[$i]} "
                ;; 
                x01)
                    echo -n "${x01[$i]} "
                ;; 
                x02) 
                    echo -n "${x02[$i]} "
                ;; 
                x03)
                    echo -n "${x03[$i]} "
                ;;  
                x04)
                    echo -n "${x04[$i]} "
                ;;  
                x05)
                    echo -n "${x05[$i]} "
                ;;  
                x06)
                    echo -n "${x06[$i]} "
                ;;  
                x07)
                    echo -n "${x07[$i]} "
                ;;  
                x08)
                    echo -n "${x08[$i]} "
                ;;  
                x09)
                    echo -n "${x09[$i]} "
                ;;  
                x10)
                    echo -n "${x10[$i]} "
                ;;  
                x11)
                    echo -n "${x11[$i]} "
                ;;  
                x12)
                    echo -n "${x12[$i]} "
                ;;  
                x13)
                    echo -n "${x13[$i]} "
                ;;                 
                x14)
                    echo -n "${x14[$i]} "
                ;; 
                x15)
                    echo -n "${x15[$i]} "
                ;; 
            esac   
        done
        echo 
    done
    echo "${lifeNow[0]} ${lifeNow[1]} ${lifeNow[2]}"
}


move(){

    heroX=${locationHero[0]}

    case ${locationHero[1]} in
        "0")
            x00[$heroX]="."
        ;;
        "1")
            x01[$heroX]="."
        ;;
        "2")
            x02[$heroX]="."
        ;;
        "3")
            x03[$heroX]="."
        ;;
        "4")
            x04[$heroX]="."
        ;;
        "5")
            x05[$heroX]="."
        ;;
        "6")
            x06[$heroX]="."
        ;;
        "7")
            x07[$heroX]="."
        ;;
        "8")
            x08[$heroX]="."
        ;;
        "9")
            x09[$heroX]="."
        ;;
        "10")
            x10[$heroX]="."
        ;;
        "11")
            x11[$heroX]="."
        ;;
        "12")
            x12[$heroX]="."
        ;;
        "13")
            x13[$heroX]="."
        ;;
        "14")
            x14[$heroX]="."
        ;;
        "15")
            x15[$heroX]="."
        ;;
    esac

    case $input in
        "w")
            locationHero[1]=$(($((${locationHero[1]}))-$((1))))
        ;;
        "a")
            locationHero[0]=$(($((${locationHero[0]}))-$((1))))
        ;;
        "s")
            locationHero[1]=$(($((${locationHero[1]}))+$((1))))
        ;;
        "d")
            locationHero[0]=$(($((${locationHero[0]}))+$((1))))
        ;;
    esac

    heroX=${locationHero[0]}
    heroY=${locationHero[1]}

    case ${locationHero[1]} in
        "0")
            x00[$heroX]="#"
        ;;
        "1")
            x01[$heroX]="#"
        ;;
        "2")
            x02[$heroX]="#"
        ;;
        "3")
            x03[$heroX]="#"
        ;;
        "4")
            x04[$heroX]="#"
        ;;
        "5")
            x05[$heroX]="#"
        ;;
        "6")
            x06[$heroX]="#"
        ;;
        "7")
            x07[$heroX]="#"
        ;;
        "8")
            x08[$heroX]="#"
        ;;
        "9")
            x09[$heroX]="#"
        ;;
        "10")
            x10[$heroX]="#"
        ;;
        "11")
            x11[$heroX]="#"
        ;;
        "12")
            x12[$heroX]="#"
        ;;
        "13")
            x13[$heroX]="#"
        ;;
        "14")
            x14[$heroX]="#"
        ;;
        "15")
            x15[$heroX]="#"
        ;;
    esac

}

countDmg=3

hero_life(){
    if (($heroY == "${enemy_1[1]}" && $heroX == "${enemy_1[0]}" || $heroY == "${enemy_2[1]}" && $heroX == "${enemy_2[0]}" || $heroY == "${enemy_3[1]}" && $heroX == "${enemy_3[0]}")); then
        countDmg=$(($(($countDmg))-$((1))))
        unset lifeNow["$countDmg"]  
    fi
}

WIN=("15" "15")

drow
while :
do
    read -n 1 -s input
    move
    hero_life
    clear
    drow
    if (($heroY == "${WIN[1]}" && $heroX == "${WIN[0]}")); then
        echo "WIN"
        break
    fi
    if (($countDmg == 0)); then
        echo "DEAD"
        break
    fi
done